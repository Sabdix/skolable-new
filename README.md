# skolable-new

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Run your tests

```
npm run test
```

### Lints and fixes files

```
npm run lint
```

### Run your end-to-end tests

```
npm run test:e2e
```

### Run your unit tests

```
npm run test:unit
```

## Important

###Importing Jquery

For importing and using some plugins from jQuery, inside the function `mounted()` , you hay to do the action of importing the plugin.

### Importing CSS Files

Some CSS Files have conflict with each other, specially with the animated CSS. Because the project is a single html file, between components, the CSS files are merged in to one.
For the CSS files have effect only in a specific component you have to use this:

```
<style src="../../assets/css/animate.min.css" scoped>
</style>
```

That only work for local css files.
