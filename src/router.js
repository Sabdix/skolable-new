import Vue from "vue";
import Router from "vue-router";
import Index from "./views/index/Index.vue";
import Login from "./views/login/Login.vue";
import Register from "./views/registro/Registro.vue";

Vue.use(Router);

export default new Router({
	routes: [
		{
			path: "/",
			name: "index",
			component: Index
		},
		{
			path: "/login",
			name: Login,
			component: Login
			// route level code-splitting
			// this generates a separate chunk (about.[hash].js) for this route
			// which is lazy-loaded when the route is visited.
		},
		{
			path: "/registro",
			name: "registro",
			component: Register
		}
	],
	mode: "history"
});
