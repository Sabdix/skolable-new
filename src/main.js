import Vue from "vue";
import App from "./App.vue";
import mat from "materialize-css";
import router from "./router";
import vr from "vue-resource";
import sa from "vue-sweetalert2";

Vue.config.productionTip = false;
Vue.use(vr);
Vue.use(mat);
Vue.use(sa);
export const eventBus = new Vue();

new Vue({
	router,
	render: h => h(App)
}).$mount("#app");
